const textInput = document.getElementById('textInput');
const chat = document.getElementById('chat');

let context = {};

const templateChatMessage = (message, from) => `
  <div class="from-${from}">
    <div class="message-inner">
      <p>${message}</p>
    </div>
  </div>
  `;

const InsertTemplateInTheChat = (template) => {
    const div = document.createElement('div');
    div.innerHTML = template;

    chat.appendChild(div);
};

const getWatsonMessageAndInsertTemplate = async(text = '') => {
    const uri = 'http://localhost:3000/';

    const response = await (await fetch(uri, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            text,
            context,
        }),
    })).json();

    context = response.result.context;
    let template;
    // Variavel pra armazenar caso a quantidade de items seja maior que ZERO
    let moreTxt = [];
    if (response.result.output.generic.length > 0) {
        //  Faz um for para add os items no array
        response.result.output.generic.forEach(data => {
            moreTxt.push(templateChatMessage(data.text, 'watson'));
        });
        // armazena na varivel o array
        template = moreTxt;
    } else {
        // Caso seja um unico item ja define na variavel template
        template = templateChatMessage(response.result.output.text, 'watson');
    }

    // Insere na div do char
    InsertTemplateInTheChat(template);

    // Auto Scroll ao final 
    var elem = document.getElementById('chat');
    elem.scrollTop = elem.scrollHeight;
};

textInput.addEventListener('keydown', (event) => {
    if (event.keyCode === 13 && textInput.value) {
        getWatsonMessageAndInsertTemplate(textInput.value);

        const template = templateChatMessage(textInput.value, 'user');
        InsertTemplateInTheChat(template);

        textInput.value = '';
    }
});

getWatsonMessageAndInsertTemplate();