'use strict'

const express = require('express')
const bodyParser = require('body-parser')
const Assistant = require('ibm-watson/assistant/v1')
const { IamAuthenticator } = require('ibm-watson/auth')
const app = express()

app.use(bodyParser.json())
app.use(express.static('./public'))

const assistant = new Assistant({
    version: '2019-02-28',
    authenticator: new IamAuthenticator({
        apikey: 'QR0BgzbdkxdLBbO8vUgie4Rhrb1xsO0cjj8j8AC9angM'
    }),
    // url: 'https://gateway.watsonplatform.net/assistant/api/v1/workspaces/239767c2-d57a-4453-9a09-fea280ceb285/message'
    url: 'https://gateway.watsonplatform.net/assistant/api'
})

app.post('/', (req, res) => {
    const { text, context = {} } = req.body;
  
    const params = {
      input: { text },
      workspaceId:'de454fb3-9334-4229-a47f-6fafcfb9c029',
      context,
    };
  
    assistant.message(params, (err, response) => {
      if (err) {
        console.error(err);
        res.status(500).json(err);
      } else {
        res.json(response);
      }
    });
  })

app.listen(process.env.PORT || 3000, () => { console.log('API OK') })
